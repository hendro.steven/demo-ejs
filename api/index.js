const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

console.log(__dirname);
app.use(express.static("public")); // mengakses file statis di direktori public
app.set("view engine", "ejs"); // menggunakan EJS sebagai template engine
app.set("views", "./views/pages"); // lokasi direktori views (optional, jika menggunakan direktori selain default)

const data = {
  title: "Halaman Utama",
  message: "Hello World!",
};

// halaman index
app.get("/", (req, res) => {
  res.render("index", { data });
});

// halaman about
app.get("/about", (req, res) => {
  res.render("about", { data });
});

app.listen(port, () =>
  console.log(
    `Server berjalan pada port ${port}; ` +
      "tekan Ctrl-C untuk menghentikan Server..."
  )
);
